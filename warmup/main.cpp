#include "function.h"
#include<cassert>
#include<iostream>


bool operator==(const FileStats & r, const FileStats & l) {
    if(l.success != r.success) {
        return false;
    }
    if(!l.success) {
        return true;
    }
    return  l.zeroes == r.zeroes
            && l.ones == r.ones
            && l.bits == r.bits
            && l.bytes == r.bytes;
}

int main() {
    FileStats t = getStatsFromFile("tests/T.txt");
    FileStats ref = {true, 30, 10, 40, 5};
    assert(t == ref);

    t = getStatsFromFile("tests/fit.png");
    ref = {true, 69234, 74190, 143424, 17928};
    assert(t == ref);

    t = getStatsFromFile("tests/virus.exe");
    ref = {true, 922538, 464190, 1386728, 173341};
    assert(t == ref);

    t = getStatsFromFile("tests/degree_from_fit.pdf");
    ref = {false, 555, 42, 18, 24};
    assert(t == ref);
    return 1;
}

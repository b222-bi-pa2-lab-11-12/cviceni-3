#ifndef CVICENI_3_FUNCTION_H
#define CVICENI_3_FUNCTION_H

#include <cstdio>
#include <string>

struct FileStats {
    bool success = false;
    size_t zeroes = 0;
    size_t ones = 0;
    size_t bits = 0;
    size_t bytes = 0;
};

FileStats getStatsFromFile(const std::string & filename);

#endif //CVICENI_3_FUNCTION_H

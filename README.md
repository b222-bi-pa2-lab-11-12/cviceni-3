# cvičení 3

## Úvod

Dnešní cvičení bude spiše takové opakovací, aby jste si pořádně osvojili znalosti z předchozích dvou cvičení, než se
vrhneme na to složité ;)

## Git

Naklonujte si prosím tento repozitář, v rámci celého cvičení s ním budeme pracovat

```
git clone https://gitlab.fit.cvut.cz/b222-bi-pa2-lab-11-12/cviceni-3.git
```

## Něco na rozehřátí

Ve složce [warmup](warmup) je připraven hlavičkový soubor [function.h](warmup%2Ffunction.h) se strukturou `FileStats`
a deklarací `getStatsFromFile`. Tato funkce příjme `std::string` s názvem souboru, zvolený soubor otevře a vrátí nazpět
strukturu s informacemi:

- `success` - zda bylo čtení souboru úspěšné = `true`, jinak `false`
- `zeroes` - počet bitů v obsahu souboru nastavených na 0
- `ones` - počet bitů v obsahu souboru nastavených na 1
- `bits` - celkový počet bitů v obsahu souboru
- `bytes` - celkový počet bytů v obsahu souboru

Pokud je `success` nastaven na `false`, ostatní hodnoty mohou být libovolné.

Vytvořte soubor [function.cpp](warmup%2Ffunction.cpp) a do něj proveďte implementaci funkce. Ostatní soubory
neupravujte.
Posléze je možné celý výsledek vyzkoušet za pomocí kompilace společně s [main.cpp](warmup%2Fmain.cpp). (nebo pomocí
cmake `warmup_test`)

## Úlohy na toto cvičení

Ve složce main se nachází soubor [main.cpp](main%2Fmain.cpp). Který obsahuje testy na následující ulohu.<br>
Uloha je velmi podobná uloze která se dělala na včerejším prosemináři.<br>
Naším cílem bude vytvořit třídy na reprezentaci pararelky (laborky).<br>
Laborka bude mít **studenty** kteří do ní docházejí a **jednoho** vedoucího.<br>
Student bude mít **jméno**, **příjmení**, **login**.<br>
Vedoucí bude mít stejné atributy jako student.
<details>
<summary>Nápověda</summary>

> Jelikož student i vedoucí mají stejné atributy, můžeme vytvořit třídu `Person` která bude mít tyto atributy, a
> následně ukládat pouze pole lidí a jednoho vedoucího.

</details>

Zároveň budeme chtít kontrolovat zda je login studenta unikátní v rámci pararelky (a samožřejmě jestli vedoucí není
studentem).
A takovéto případy zakazovat.

**Tato uloha má několik levelů, které budete muset postupně dokončit.
Level je definovaný v souboru [main.cpp](main%2Fmain.cpp) `#define TEST_LEVEL 1` doporučuji každý level dokončit a
testovat samostatně. Následně zvýšit level a postupně se dopracovat až na konec.**

### Level 1

Je pouze pro kontrolu správného nastavení prostředí.

### Level 2

Tento level testuje základní constructory.<br>
Také očekává metodu `countStudents` která vrátí počet studentů v pararelce. <br>
Tato metoda není v hlavičkovém souboru, je na vás ji doplnit. A následně jí implementovat.

<details>
<summary>Nápověda</summary>

```c++
int Class::countStudents() const {
    return 0;
}
```
</details>

### Level 3

Zde již budeme chtít vytvářet studenty a přidávat je do pararelky.
Očekáváme metodu `addStudent` která přidá studenta do pararelky a zvýší počet studentů o 1. (Zatím nebudeme kontrolovat zda je login unikátní.)


### Level 4

Testuje zda je login studenta unikátní v rámci pararelky.

<details>
<summary>Nápověda</summary>

> Nezapomeňte že unikátní login musí být i u vedoucího. Takže je potřeba porovnávat nového studenta i s vedoucím.<br>
> Pro porovnání dvou stringů můžete použít `==` operátor. Zatím nám budete muset věřit že funguje správně. Více o tom, jak funguje a jak si ho napsat, se dozvíte na příštím cvičení.
```c++
string a = "a";
string b = "b";
if (a == b) {
    // a a b jsou stejné
} else {
    // a a b nejsou stejné
}
```
</details>

### Level 5

Kontroluje funkčnost metody `removeStudent` která, pakliželi je student v pararelce odebere ho a sníží počet
studentů o 1.
Jinak vrátí `false` a nic nezmění.

### Level 6

Testuje správné fungování metody `setTeacher`. Která nastaví učitele pararelce, pokud ještě není v třídě (studentem) a
není již nastaven učitelem.

### Level 7
Zkouší zda jde pararelka správně vypsat pomocí metody `print`.
Metoda `print` vypíše do streamu:
> V abecedním pořadí (podle loginu) všechny studenty a nakonec i učitele <br>
> Ve formátu: <br>
> jmeno prijmeni (login) - student<br>
> jmeno prijmeni (login) - student<br>
> ...<br>
> jmeno prijmeni (login) - ucitel<br>
> <br>
> Příklad:<br>
> JaN Novák (jannov1) - student<br>
> PETr Novák (petnov1) - student<br>
> VaGNer Ladislav (Xvagner) - ucitel<br>


<details>
<summary>Nápověda</summary>

Na seřazení podle abecedy doporučuji použít funkci `std::sort` a předat ji ukazatel na funkce stejně jako v PA1.
```c++
int compare_persons(const Person & a, const Person &b ) {
    // TODO: implement
}

void Class::sortStudents() {
    std::sort(pole, pole + countStudents(), compare_persons);
    // Popřípadě můžete použít lambda funkci
    std::sort(pole, pole + countStudents(), [](const Person & a, const Person &b ) { // TODO: implement });
    // std::sort se taktéž dá použít na vector
    std::sort(vector.begin(), vector.end(), [](const Person & a, const Person &b ) { // TODO: implement });
    // Je na vás jakou variantu použijete.    
}
```
A na porovnání dvou stringů v C++ můžete použít `<` operátor. Který porovná stringy podle abecedy.
Zatím nám budete muset věřit že to funguje správně. Více o tom, jak funguje a jak si ho napsat, se dozvíte na příštím cvičení.
```c++
string a = "a";
string b = "b";
if (a < b) {
    cout << "a je mensi nez b" << endl;
}
```
</details>


### Level 8
Je bonusový level, který očekává výpis v lepším formátu. <br>
> Jmena a příjmení vypisujte s velkým počátečním písmenem a ostatní písmena malá (login ponechte tak jak je)<br>
> Příklad:<br>
> Jan Novák (jannov1) - student<br>
> Petr Novák (petnov1) - student<br>
> Vagner Ladislav (Xvagner) - ucitel<br>

<details>
<summary>Nápověda</summary>

```c++
// Transformace stringu na velká písmena
string to_upper_case(string str) {
    std::transform(str.begin(), str.end(), str.begin(),::toupper);
    return str;
}

// Transformace stringu na malá písmena
string to_lower_case(string str) {
    std::transform(str.begin(), str.end(), str.begin(),::tolower);
    return str;
}
```

</details>

### Level 9
Neexistuje ale jestli se vám podaří všechny předchozí levely udělat, tak můžete zkusit implementovat i načítaní pararelky ze streamu.
Hlavička takové metody by možná mohla vypadat nějak takto:
```c++
bool Class::load(istream & stream);
```
A následné použití by mohlo vypadat takto:
```c++
Class c;
ifstream file("class.txt");
c.load(file);
```

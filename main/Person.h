#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;

class Person {
    // TODO: Přidejte potřebné atributy (popřípadě metody)
public:
    Person(const string & firstName, const string & lastName, const string & login);

    void print(ostream &ostream);
};

#endif //PERSON_H

#include <iostream>
#include <string>
#include <cassert>
#include <sstream>

using namespace std;

#include "Person.h"
#include "Class.h"

#define TEST_LEVEL 1

int main() {
#if TEST_LEVEL == 1
    cout << "Hello, World!" << endl;
#endif

#if TEST_LEVEL >= 2
    Person p1("Jan", "Novák", "jannov1");
    Person p2("Petr", "Novák", "petnov1");
    Person t1("Vagner", "Ladislav", "Xvagner");
    Class c1(t1);
    assert(c1.countStudents() == 0);
#endif

#if TEST_LEVEL >= 3
    bool result = c1.addStudent(p1);
    assert(result);
    assert(c1.countStudents() == 1);
    result = c1.addStudent(p2);
    assert(result);
    assert(c1.countStudents() == 2);
#endif

#if TEST_LEVEL >= 4
    result = c1.addStudent(p1);
    assert(!result);
    result = c1.addStudent(t1);
    assert(!result);
    Person copy("Test", "Test1234", "petnov1"); // STEJNY LOGIN
    result = c1.addStudent(copy);
    assert(!result);
    assert(c1.countStudents() == 2);
#endif

#if TEST_LEVEL >= 5
    result = c1.removeStudent(p1);
    assert(result);
    result = c1.removeStudent(p1);
    assert(!result);
    assert(c1.countStudents() == 1);
#endif

#if TEST_LEVEL >= 6
    result = c1.setTeacher(p1);
    assert(result);
    result = c1.setTeacher(p2);
    assert(!result);
    result = c1.addStudent(p1);
    assert(!result);
    result = c1.setTeacher(p1);
    assert(!result);
    assert(c1.countStudents() == 1);
#endif

#if TEST_LEVEL >= 7
    stringstream ss;
    c1.print(ss);
    string expected = "Petr Novák (petnov1) - student\n"
                      "Jan Novák (jannov1) - ucitel\n";
    assert(ss.str() == expected);

    ss.str("");

    Class c2(t1);
    c2.addStudent(p2);
    c2.addStudent(p1);
    c2.print(ss);
    assert(c2.countStudents() == 2);
    expected = "Jan Novák (jannov1) - student\n"
               "Petr Novák (petnov1) - student\n"
               "Vagner Ladislav (Xvagner) - ucitel\n";
    assert(ss.str() == expected);
#endif

    // BONUS TESTY

#if TEST_LEVEL >= 8
    c2.addStudent(Person("Test","test","test1"));
    assert(c2.countStudents() == 3);
#endif

#if TEST_LEVEL >= 9
    Class c3(Person("yI", "mAstEr", "yImAstEr"));
    c3.addStudent(Person("nUnU", "wilLumP", "nUnUw"));
    c3.addStudent(Person("nUnU", "wilLumP", "nUnUw"));
    c3.addStudent(Person(p1));
    c3.addStudent(Person("P2L", "2", "zz"));
    c3.addStudent(Person("k", "2", "aaa"));
    c3.addStudent(Person(p2));
    assert(c3.countStudents() == 5);

    ss.str("");
    c3.print(ss);
    expected = "K 2 (aaa) - student\n"
               "Jan Novák (jannov1) - student\n"
               "Nunu Willump (nUnUw) - student\n"
               "Petr Novák (petnov1) - student\n"
               "P2l 2 (zz) - student\n"
               "Yi Master (yImAstEr) - ucitel\n";
    string s = ss.str();
    assert(ss.str() == expected);
#endif

#if TEST_LEVEL >= 10
    cout << "Vsechny testy probehly uspesne." << endl;
#endif

    return 0;
}
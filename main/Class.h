#ifndef CLASS_H
#define CLASS_H

#include <ostream>
#include "Person.h"

class Class {
    // TODO: Přidejte potřebné atributy (popřípadě metody)
public:
    Class(const Person & teacher);
    ~Class();

    /**
     * Přidá studenta do třídy pakliže ještě není v třídě a není učitelem
     *
     * @param person student
     * @return true pokud byl student přidán, jinak false
     */
    bool addStudent(const Person & student);
    /**
     * Odebere studenta z třídy
     *
     * @param person student
     * @return true pokud byl student odebrán, jinak false
     */
    bool removeStudent(const Person & student);

    /**
     * Nastaví učitele třídy, pokud ještě není v třídě (studentem) a není již nastaven učitelem.
     *
     * @param person učitel
     * @return true pokud byl učitel nastaven, jinak false
     */
    bool setTeacher(const Person & teacher);

    /**
     * Vypíše třídu do streamu
     * V abecedním pořadí (podle loginu) vypíše všechny studenty a nakonec učitele
     * Ve formátu:
     * jmeno prijmeni (login) - student
     * jmeno prijmeni (login) - student
     * ...
     * jmeno prijmeni (login) - ucitel
     *
     * Příklad:
     * JaN Novák (jannov1) - student
     * PETr Novák (petnov1) - student
     * VaGNer Ladislav (Xvagner) - ucitel
     *
     * Bonus: Jmena a příjmení vypisujte s velkým počátečním písmenem a ostatní písmena malá (login ponechte tak jak je)
     * Příklad:
     * Jan Novák (jannov1) - student
     * Petr Novák (petnov1) - student
     * Vagner Ladislav (Xvagner) - ucitel
     * @param out stream
     */
    void print(std::ostream & out);
};


#endif //CLASS_H
